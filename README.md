## ansible4csdev

The following playbooks assisting a developer to his own devel environment over aws


### Install  

git clone https://gitlab.com/yarboa/ansible4csdev.git

Prepare your own virtualenv

1. python -m venv /tmp/.venv
1. source /tmp/.venv/bin/activate
1. pip install -r requirements.txt
1. ansible-galaxy install -r requirements.yaml


### Setup  

The following bash environment variables should be configure

export AWS_ACCESS_KEY_ID='YOUR_AWS_ACCESS_KEY'  
export AWS_SECRET_ACCESS_KEY='YOUR_AWS_SECRET_KEY'



**Note:**
If you do not have keys please login AWS and create it,  

1. Go to Amazon Services >> IAM Users, 
1. Select your user, go to Security Credentials 
1. Pres Generate access key 
1. Save the AWS_ACCESS_KEY_ID,  AWS_SECRET_ACCESS_KEY

The keys are cross site, and related to user account


### Run playbooks

*To create a vm testing its connectivity and prepare dev env run:*  

ansible-playbook playbooks/site.yml


*To run playbooks in certain stages:*

ansible-playbook playbooks/ec2_install/ec2-create.yml  

ansible-playbook playbooks/ec2_install/ec2-test.yml
  
ansible-playbook playbooks/ec2_install/ec2-spindown.yml
 
ansible-playbook playbooks/ec2_install/ec2-spinup.yml  
  
ansible-playbook playbooks/meta_rpm/meta-rpm.yml


**Note:**

Override parameters with -e option  
ansible-playbook -e ec2_name_tag=my_tag1 playbooks/ec2_install/ec2-spinup.yml
or set permanent variables here:  
roles/ec2_install/ec2-create/defaults/main.yml 

*ssh to vm*  
Please refer VM SSH Keypairs below  
ssh -i ~/.ssh/id_rsa.pub ec2-user@\<regitered host vars output\>  

#### Playbooks variables

Important parameters related to AWS region
aws_region: us-east-2  
ec2_instance_type: t2.micro  
ec2_ami_id: ami-0023b746ecfab8fa3  
ec2_volume_size: 60  
ec2_keypair_name: yarboa-keys  
ec2_security_group: base-enablement  
ec2_name_tag: meta-rpm  
ec2_contact_tag: ansible_deploy  
ec2_app_tag: meta-rpm  


**Note:**

Playbooks tested with the following aws images  
Default is x86_64, due to aarch64 cost difference  

*CS9 x86_64:*  

ec2_ami_id: ami-0023b746ecfab8fa3  
ec2_instance_type: t2.micro  

*CS9 aarch64:*  
ec2_ami_id: ami-0a49229a76aed9553  
ec2_instance_type: t4g.nano  

Additional variables exist in roles default/vars directories

### VM SSH Keypairs

Upload your keypair to AWS region you would like to run the instance  
Please contact @Auto-Team-Base gchat for that.  
Once AWS key-pair generated update ec2_keypair_name with that name.  
Could be done here:
ansible-playbook -e ec2_keypair_name=<aws_name> playbooks/ec2_install/ec2-create.yml  

### Terraform option

Please make sure your aws configure is set to the correct site:

```
aws configure
```

Terraform install on different OS distro please follow (install terraform)[1]
To control number of deployed machines please update count under:

```
resource "aws_instance"
```

To deploy servers:

```
terraform apply
```

To list deployed servers:

```
terraform state list
aws_instance.autosd-build_server[0]
aws_instance.autosd-build_server[1]
aws_instance.autosd-build_server[2]
aws_instance.autosd-build_server[3]
```

To show instance details:

```
terraform state show  aws_instance.autosd-build_server[1]
```

To show recorded output:

```
terraform output -json | jq .ec2instance_private_ip.value[0]
"172.31.13.252"
```

[1]: https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli
