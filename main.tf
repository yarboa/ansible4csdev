terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  required_version = ">= 1.2.0"
}

provider "aws" {
  region = "us-east-2"
}

resource "aws_instance" "autosd-build_server" {
  count = 4
  ami                         = "ami-0a49229a76aed9553"
  instance_type               = "t4g.nano"
  associate_public_ip_address = true
  vpc_security_group_ids      = ["sg-8dcb48c4"]
  key_name                    = "yarboa-keys"
  tags = {
    Name = "AustoSD-aarch64-${count.index}"
    App  = "meta-rpm"
  }
}

output "ec2instance_public_ip" {
  value = aws_instance.autosd-build_server.*.public_ip
}
output "ec2instance_private_ip" {
  value = aws_instance.autosd-build_server.*.private_ip
}
output "ec2instance_id" {
  value = aws_instance.autosd-build_server.*.id
}

